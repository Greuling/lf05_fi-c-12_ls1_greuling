import java.util.Scanner;
public class Quadrieren {
   
	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		// (E) "Eingabe"
		// Wert f�r x festlegen:
		// ===========================
		System.out.println("Dieses Programm berechnet die Quadratzahl x�");
		System.out.println("---------------------------------------------");
		System.out.println("Eine Zahl bitte: ");
		double x = getUserInput();
				
		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// ================================
		double ergebnis= quadrieren(x);

		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		System.out.printf("x = %.2f und x�= %.2f\n", x, ergebnis);
		scn.close();
	}	
	
	public static double getUserInput() {
		Scanner scn = new Scanner(System.in);
		double input = scn.nextDouble();
		return input;
	}
	public static double quadrieren(double x) {
		double erg = x*x;
		return erg;
		
	}
	
}
