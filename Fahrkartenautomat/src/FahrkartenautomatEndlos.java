import java.util.Scanner;
class FahrkartenautomatEndlos{
    public static void main(String[] args){
       double zuZahlenderBetrag; 
       double r�ckgabebetrag;
       while(true) {
    	   //Start der Bestell erfassung
    	   zuZahlenderBetrag = fahrkartenbestellungErfassen();     
    	   // Geldeinwurf
    	   // -----------
    	   r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
    	   // Fahrscheinausgabe
    	   // -----------------
    	   fahrkartenAusgeben();
    	   // R�ckgeldberechnung und -Ausgabe
    	   // -------------------------------
    	   rueckgeldAusgeben(r�ckgabebetrag);
    	   System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
    	   System.out.println(" ");
    	   System.out.println(" ");
           }
    }
        
    public static double fahrkartenbestellungErfassen() {
    	Scanner scn = new Scanner(System.in);
    	double zuZahlenderBetrag = 0;
    	double anzahlTickets;
    	int wahl;
    	// Bestell vorgang
    	System.out.println("Fahrkartenbestellvorgang:");
    	System.out.println("=========================");
    	System.out.println("W�hlen sie Ihr Wunschticket f�r Berlin AB aus: ");
    	System.out.println("	 Regeltarif AB [2,90 EUR]			(1)");
    	System.out.println("Tageskarte Regeltarif AB [8,60 EUR] 				(2)");
    	System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR]	 	(3)");
    	System.out.print("Ihre Wahl: ");
    	wahl=scn.nextInt();
    	//kontrolle der Auswahl
    	 while(wahl < 1 || wahl > 3) {
    		 System.out.println("falsche Eingabe");
    		 System.out.print("Ihre Wahl: ");
    		 wahl=scn.nextInt();    		 
    	 }
    	 // Nach der Wahl den zu zahlenden Betrag festlegen (pro ticket)
    	 
    	switch(wahl) {
    	case 1:
    		zuZahlenderBetrag= 2.9;
    		break;
    	case 2:
    		zuZahlenderBetrag= 8.6;
    		break;
    	case 3:
    		zuZahlenderBetrag= 23.50;
    		break;
    	}
    	    	
        System.out.print("Wie viele Tickets wollen Sie? (maximal 10)");
        anzahlTickets = scn.nextInt();
        
        //kontrolle anzahlTickets
        while(anzahlTickets < 0 || anzahlTickets > 10) {
        	System.out.println("Falsche Anzahl. Bitte erneut eingeben");
        	anzahlTickets = scn.nextInt();
        }
        zuZahlenderBetrag *= anzahlTickets;
        return zuZahlenderBetrag;
    	
    }
   
    public static double fahrkartenBezahlen(double zuZahlen) {
    	Scanner scn2 = new Scanner(System.in);
    	double r�ckgabeBetrag;
    	double eingezahlterGesamtbetrag = 0.0;
    	double eingeworfeneM�nze;
    	
    	while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.printf("Noch zu zahlen: %.2f �\n", (zuZahlen - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   eingeworfeneM�nze = scn2.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
    	
    	r�ckgabeBetrag = eingezahlterGesamtbetrag - zuZahlen;
    	
    	return r�ckgabeBetrag;
    	
    }

    public static void fahrkartenAusgeben() {
    	 System.out.println("\nFahrschein wird ausgegeben");
         for (int i = 0; i < 8; i++)
         {
            System.out.print("=");
            try {
  			Thread.sleep(250);
  		} catch (InterruptedException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
         }
         System.out.println("\n\n");    	
    }
    
    public static void rueckgeldAusgeben(double r�ckgabebetrag) {
    	if(r�ckgabebetrag > 0.0)
        {
     	   System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
         	  System.out.println("2 EURO");
 	          r�ckgabebetrag -= 2.0;
            }
            while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
         	  System.out.println("1 EURO");
 	          r�ckgabebetrag -= 1.0;
            }
            while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
         	  System.out.println("50 CENT");
 	          r�ckgabebetrag -= 0.5;
            }
            while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
         	  System.out.println("20 CENT");
  	          r�ckgabebetrag -= 0.2;
            }
            while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
         	  System.out.println("10 CENT");
 	          r�ckgabebetrag -= 0.1;
            }
            while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
         	  System.out.println("5 CENT");
  	          r�ckgabebetrag -= 0.05;
            }
        }
    }
}